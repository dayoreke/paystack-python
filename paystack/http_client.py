try:
    import requests
except ImportError:
    requests = None
    print 'Requests Not installed'
    exit()

class HTTPClient(object):
    def __init__ (self):
        pass

    def request(self, method, url, headers, data= None):
        pass

class RequestsClient(HTTPClient):
    def request(self, method, url, headers, data = None):
        try:
            return requests.request(method, url, headers = headers, data = data)
        except TypeError as e:
            return e

