import paystack, json
from paystack.http_client import RequestsClient

def populate_headers(secret_key):
    if secret_key is not None:
        return {"Authorization": secret_key, "Content-Type": "application/json"}


class TransactionInterface(object):
    def __init__(self):
        self.headers = populate_headers(paystack.secret_key)
        self.class_url = paystack.api_url + '/transaction/'

    def initialize(self):
        pass

    def charge(self):
        pass

    def verify(self):
        pass

class Transaction(TransactionInterface):

    def initialize(self, **params):
        url = self.class_url + 'initialize'
        response = RequestsClient().request('post', url, self.headers, json.dumps(params))
        return response


    def verify(self, reference):
        url = self.class_url + 'verify/' + reference
        response = RequestsClient().request('get', url, self.headers)
        return response

    def charge(self, **params):
        url = self.class_url + 'charge_authorization'
        response = RequestsClient().request('post', url, self.headers, json.dumps(params))
        return response

