import sys

class PaystackError(Exception):
    pass

class ApiConnectionError(PaystackError):
    pass

class AuthenticationError(PaystackError):
    pass